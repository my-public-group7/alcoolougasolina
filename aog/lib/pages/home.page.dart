import 'package:aog/widgets/logo.widget.dart';
import 'package:aog/widgets/submit-form.dart';
import 'package:aog/widgets/success.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Color _color = Colors.blue;

  var _gasCrtl = new MoneyMaskedTextController();

  var _alcCrtl = new MoneyMaskedTextController();

  var _busy = false;

  var _completed = false;

  var _resultText = "Abasteça com álcool!";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: AnimatedContainer(
        duration: Duration(seconds: 1),
        color: _color,
        child: ListView(children: <Widget>[
          Logo(),
          _completed
              ? Success(reset: reset, result: _resultText)
              : SubmitForm(
                  alcCrtl: _alcCrtl,
                  gasCrtl: _gasCrtl,
                  submitFunc: calculate,
                  busy: _busy,
                ),
        ]),
      ),
    );
  }

  Future calculate() {
    double alc = double.parse(
          _alcCrtl.text.replaceAll(new RegExp(r'[,.]'), ''),
        ) /
        100;
    double gas = double.parse(
          _gasCrtl.text.replaceAll(new RegExp(r'[,.]'), ''),
        ) /
        100;
    double res = alc / gas;

    setState(() {
      _completed = false;
      _busy = true;
    });

    return new Future.delayed(const Duration(seconds: 1), () {
      setState(() {
        if (res >= 0.7) {
          _resultText = "Abasteça com Gasolina!";
          _color = Colors.yellow[800];
        } else {
          _resultText = "Abasteça com Álcool!";
          _color = Colors.blue[200];
        }

        _completed = true;
        _busy = false;
      });
    });
  }

  reset() {
    setState(() {
      _gasCrtl = new MoneyMaskedTextController();
      _alcCrtl = new MoneyMaskedTextController();
      _completed = false;
      _busy = false;
      _color = Theme.of(context).primaryColor;
    });
  }
}
